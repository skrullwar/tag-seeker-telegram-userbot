from dataclasses import dataclass


@dataclass
class TelegramData:

    def __init__(self, api_id=None, api_hash=None):
        self.api_id = api_id
        self.api_hash = api_hash
