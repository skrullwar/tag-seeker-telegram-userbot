from dataclasses import dataclass


@dataclass
class UserConfig:

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.__get_lists()

    def __get_lists(self):
        for key, value in self.__dict__.items():
            if type(value) == str and '", "' in value:
                setattr(self, key, eval(value))


if __name__ == "__main__":
    usr = UserConfig(
        first_name='giovanni',
        age=13, last_name='paoli',
        keywords='example; free; several words in a tag; last; tag',
        fav_food='hamburger, pizza, pasta, nutella',
        fav_drinks='("water", "cola")',
        skills='(nothing, nada, nisba)'
    )
    print('\n', usr.__dict__)
    for k in usr.fav_drinks:
        print(k)
