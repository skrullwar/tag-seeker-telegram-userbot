from dataclasses import dataclass
from config.subclasses.telegram_data import TelegramData
from config.subclasses.user_config import UserConfig
import configparser


@dataclass
class ConfigWrapper(configparser.ConfigParser):
    """Class useful to manage your config"""

    telegram_config = TelegramData()
    user_config: UserConfig

    def __init__(self, path: str):
        super().__init__()
        self.path = path

    def read(self, filename=None, encoding=None):
        super().read(self.path if not filename else filename)
        self.telegram_config.__dict__ = self._sections[TelegramData.__name__]
        self.user_config = UserConfig(**self._sections[UserConfig.__name__])
        # print('s : ', self._sections[TelegramData.__name__])


if __name__ == '__main__':
    conf = ConfigWrapper('config.ini')
    conf.read()
    print(conf.telegram_config.__dict__)
    print(conf.user_config.__dict__)
