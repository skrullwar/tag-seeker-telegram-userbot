# Tag Seeker Telegram Userbot

A telegram userbot that wait for a specific word or tag   in your chats and forward the messages in your private chat/group

# Usage:
To use this bot you just need to install Docker and
Build the app with `docker build -t mybot .` and run it with the command:
`docker run -ti mybot`