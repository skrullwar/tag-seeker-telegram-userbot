from telethon import TelegramClient, events
from config import ConfigWrapper

config = ConfigWrapper('config/config.ini')
config.read()

client = TelegramClient('anon', config.telegram_config.api_id, config.telegram_config.api_hash)
client.start()


@client.on(events.NewMessage)
async def main(event):
    if event.message:
        for key in config.user_config.keywords:
            if key in event.message.message.lower():
                await event.forward_to(config.user_config.destination_chat)
                if config.user_config.debug:
                    await client.send_message(config.user_config.destination_chat, f'captured message with '
                                                                                   f'keyword : {key}')
                    print(f'captured message with keyword : {key}')


if __name__ == "__main__":
    client.add_event_handler(main)
    client.run_until_disconnected()
