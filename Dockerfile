# set base image (host OS)
FROM python:3.8
# set the working directory in the container
WORKDIR /code

# copy the dependencies file to the working directory
COPY requirements.txt .
COPY LICENSE.txt .
COPY README.md .
COPY Main.py .

# install dependencies
RUN pip install -r requirements.txt

# copy the content of the local src directory to the working directory
COPY config/ ./config

# command to run on container start
CMD [ "python", "./Main.py" ]